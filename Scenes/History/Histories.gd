extends Node

onready var historiesAvailable = [$HistoryOne, $HistoryTwo, $HistoryThree, $HistoryFour, $HistoryFive, $HistorySix]
onready var historiesSelected = []

func getHistory():
	historiesAvailable.shuffle()
	var historySelected = historiesAvailable.pop_front()
	historiesSelected.append(historySelected)
	return historySelected

func getBossHistory():
	return $HistoryBoss
