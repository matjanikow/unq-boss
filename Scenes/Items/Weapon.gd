extends Node2D

export(int) var attack = 3

onready var area = $Area2D
onready var collision = $Area2D/CollisionShape2D

var itemNames = {
	0: "Rota",
	1: "Oxidada",
	2: "Comun",
	3: "Reforzada",
	4: "Epica"
}

func _ready():
	attack = int(rand_range(0,4)) 
	var itemName = "Espada %s" % itemNames[attack]
	var itemAttack = "Ataque +%s" % attack
	$ItemInfoControl.loadValues(itemName, itemAttack)
	
func damage():
	return attack

func _on_Area2D_body_entered(body):
	if body.is_in_group("Player"):
		self.hide()
		area.set_deferred("monitoring", false)
		area.set_deferred("monitorable", false)
		get_parent().call_deferred("remove_child",self)
		body.equip(self, Enumerate.item.weapon)

func _on_Area2DItemInfo_body_entered(body):
	if body.is_in_group("Player"):
		$ItemInfoControl.show_Info()

func _on_Area2DItemInfo_body_exited(body):
	if body.is_in_group("Player"):
		$ItemInfoControl.hide_Info()
