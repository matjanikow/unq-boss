extends "res://Scenes/Items/Weapon.gd"

func _ready():
	attack = 45
	._ready()

func damage():
	EventBus.destrolibur_used()
	return attack

func _on_Area2DItemInfo_body_entered(body):
	._on_Area2DItemInfo_body_entered(body)

func _on_Area2DItemInfo_body_exited(body):
	._on_Area2DItemInfo_body_exited(body)
