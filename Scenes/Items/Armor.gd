extends Node2D

export(int) var defense = 0
onready var area = $Area2D
onready var collision = $Area2D/CollisionShape2D

var itemNames = {
	0: "Inservible",
	1: "Mala",
	2: "Buena",
	3: "Mejorada",
	4: "Excelente"
}

func _ready():
	defense = int(rand_range(0,4)) 
	var itemName = "Armadura %s" % itemNames[defense]
	var itemDefense = "Defensa +%s" % defense
	$ItemInfoControl.loadValues(itemName, itemDefense)

func defense():
	return defense

func _on_Area2D_body_entered(body):
	if body.is_in_group("Player"):
		self.hide()
		area.set_deferred("monitoring", false)
		area.set_deferred("monitorable", false)
		get_parent().call_deferred("remove_child",self)
		body.equip(self, Enumerate.item.armor)

func _on_Area2DItemInfo_body_entered(body):
	if body.is_in_group("Player"):
		$ItemInfoControl.show_Info()

func _on_Area2DItemInfo_body_exited(body):
	if body.is_in_group("Player"):
		$ItemInfoControl.hide_Info()
