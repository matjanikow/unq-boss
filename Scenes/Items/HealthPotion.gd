extends Node2D

export(float) var health_ratio = 0.75

onready var area = $Area2D
onready var collision = $Area2D/CollisionShape2D

func _ready():
	$ItemInfoControl.loadValues("Cura un", "75% de Vida")

func health():
	return health_ratio

func _on_Area2D_body_entered(body):
	if body.is_in_group("Player"):
		body.consume(self)
		queue_free()
#		self.hide()
#		area.set_deferred("monitoring", false)
#		area.set_deferred("monitorable", false)
#		get_parent().remove_child(self)


func _on_Area2DItemInfo_body_entered(body):
	if body.is_in_group("Player"):
		$ItemInfoControl.show_Info()


func _on_Area2DItemInfo_body_exited(body):
	if body.is_in_group("Player"):
		$ItemInfoControl.hide_Info()
