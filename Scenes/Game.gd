extends Node2D

var preLoadedBossLevel = preload("res://Scenes/Level/LevelBoss/LevelBoss.tscn")
var preLoadedLevel = preload("res://Scenes/Level/Level.tscn")
var historySelector = preload("res://Scenes/UI/HistorySelection.tscn")
var statusOverlay = preload("res://Scenes/UserOverlay.tscn")

onready var can_start_turn = true
onready var game_Over = false

onready var turn = $Turn
onready var level = $Level
export var actualLevel = -1

func _ready():
	EventBus.connect("turn_end",self,"on_turn_end")
	EventBus.connect("game_over",self,"on_game_over")
	EventBus.connect("stairs_reached",self,"on_stair_reached")
	
	var player = $Player
	level.player = player
	level.nemesisTrait = $NemesisTraits
	level.init()
	turn.player = player
	turn.enemies = level.enemies
	$GameCamera.set_camera_position(player)
	player.nemesisTrait = $NemesisTraits

func _physics_process(delta):
	if not game_Over and can_start_turn and isActionInput():
		can_start_turn = false
		turn.start_turn(getActionInput())

func isActionInput():
	var input = Input
	return Input.is_action_pressed("ui_down") \
		or Input.is_action_pressed("ui_up") \
		or Input.is_action_pressed("ui_right") \
		or Input.is_action_pressed("ui_left") \
		or Input.is_action_pressed("ui_select")

func getActionInput():
	if 	Input.is_action_pressed("ui_down"):
		return Enumerate.action.down
	elif Input.is_action_pressed("ui_up"):
		return Enumerate.action.up
	elif Input.is_action_pressed("ui_right"):
		return Enumerate.action.right
	elif Input.is_action_pressed("ui_left"):
		return Enumerate.action.left
	elif Input.is_action_pressed("ui_select"):
		return Enumerate.action.idle
	else:
		return Enumerate.action.idle

func passToNextLevel():
	can_start_turn = false
	var player = $Player
	player.turn_ended = false
	
	if actualLevel == -1:
		player.set_health(150)
		$Player/HealthBar/HealthBar.max_value = 150
	actualLevel += 1
	
	var isBossLevel = actualLevel == 4
	
	var raceSelection = historySelector.instance()
	if isBossLevel:
		raceSelection.historyToLoad = $Histories.getBossHistory()
	else:
		raceSelection.historyToLoad = $Histories.getHistory()
		
	add_child(raceSelection)
	raceSelection.init()

	var levelToFree = get_tree().get_root().get_node("Game/Level")
	remove_child(levelToFree)
	levelToFree.queue_free()
	turn.enemies.clear()

	yield(EventBus,"race_selected")

	
	match raceSelection.historyType:
		Enumerate.history_Type.fear:
			player.setTraits(\
				$NemesisTraits.randomTraitWithOut(raceSelection.race_Selected),\
				raceSelection.race_Selected)
		Enumerate.history_Type.fury:
			player.setTraits(\
				raceSelection.race_Selected, \
				$NemesisTraits.randomTraitWithOut(raceSelection.race_Selected))
		Enumerate.history_Type.boss:
			player.setTraits(null,null)
	var playerStatus = statusOverlay.instance()
	add_child(playerStatus)
	playerStatus.addPLayerStatus(player)
	playerStatus.init()
	yield(EventBus,"statusOk")
	remove_child(playerStatus)
	var newLevel
	if isBossLevel:
		newLevel = preLoadedBossLevel.instance()
		newLevel.position = Vector2(0,0)
	else:
		newLevel = preLoadedLevel.instance()
	add_child(newLevel)
	newLevel.player = player
	if not isBossLevel:
		$LevelSettings.loadSettings(newLevel,actualLevel)
	newLevel.init()
	remove_child(raceSelection)
	raceSelection.queue_free()
	turn.enemies = newLevel.enemies
	$GameCamera.set_camera_position(player)

	yield(get_tree().create_timer(0.60), "timeout")
	player.turn_ended = true
	can_start_turn = true
	
func on_stair_reached():
	call_deferred("passToNextLevel") # Se llama diferenciado para que no falle

func on_turn_end():
	yield(get_tree().create_timer(0.30), "timeout")
	can_start_turn = true

func on_game_over():
	game_Over = true
