extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
onready var fade = $Fade
onready var HP = $CanvasLayer/Panel/HP#get_node("CanvasLayer/Panel/HP");
onready var Defensa = get_node("CanvasLayer/Panel/Defensa");
onready var Ataque = get_node("CanvasLayer/Panel/Ataque");
onready var Miedo = get_node("CanvasLayer/Panel/Fear");
onready var Furia = get_node("CanvasLayer/Panel/Fury");
# Called when the node enters the scene tree for the first time.

func _ready():
	fade.connect("fade_finished",self,"_on_Fade_fade_finished")
func addPLayerStatus(player):
	HP.text = str(player.health) + "/" + str(player.max_health)
	Defensa.text = str(player.calculateDefense())
	Ataque.text = str(player.attack_power + player.inventory.getEquipmentAttack())
	Miedo.text = player.fear
	if(player.fury):
		Furia.text = player.fury
	else:
		Furia.text = ""
func init():
	fade.show()
	fade.fade_Out()

func _on_Button_pressed():
	fade.show()
	fade.fade_in()
	EventBus.player_status_done()