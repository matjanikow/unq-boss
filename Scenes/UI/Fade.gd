extends ColorRect

signal fade_finished

func fade_in():
	visible = true
	$AnimationPlayer.play("FadeIn")

func fade_Out():
	$AnimationPlayer.play("FadeOut")

func _on_AnimationPlayer_animation_finished(anim_name):
	emit_signal("fade_finished",anim_name)
