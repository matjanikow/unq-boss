extends Node2D

onready var tween = $Tween

func _ready():
	modulate = 0

func loadValues(itemName, itemValue):
	$Nombre.text = itemName
	$Valor.text = itemValue

func show_Info():
	tween.interpolate_property(self, "modulate", Color(1,1,1,0), Color(1,1,1,1), 1, Tween.TRANS_SINE,Tween.EASE_IN_OUT)
	tween.start()

func hide_Info():
	tween.interpolate_property(self, "modulate", Color(1,1,1,1), Color(1,1,1,0), 1, Tween.TRANS_SINE,Tween.EASE_IN_OUT)
	tween.start()