extends Node

signal stairs_reached
signal next_level
signal turn_completed # Indica que cada character termino su turno
signal turn_end # Indica que termino el turno de la ronda
signal game_over
signal destrolibur_used
signal boss_change_type
signal make_damage
signal race_selected
signal shake_camera
signal statusOk

func shake_camera():
	emit_signal("shake_camera")

func destrolibur_used():
	emit_signal("destrolibur_used")

""" GAME """
func turn_completed():
	emit_signal("turn_completed")

func turn_end():
	emit_signal("turn_end")

func game_over():
	emit_signal("game_over")

func boss_change_type(fury,fear):
	emit_signal("boss_change_type",fury,fear)

""" LEVELS """
func stairs_reached():
	emit_signal("stairs_reached")

func next_level():
	emit_signal("next_level")
	
func race_selected():
	emit_signal("race_selected")

func player_status_done():
	emit_signal("statusOk")
