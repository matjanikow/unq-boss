extends Node

var enemies = ["rat", "dog", "aberration" ]
var floaty_text_scene = preload("res://Scenes/UI/FloatingText.tscn")
onready var hit  = get_node("../Hit");
onready var miss = get_node("../Miss");
onready var crit = get_node("../Critical");

func _ready():
	EventBus.connect("damage",self,"make_damage")
	
func make_damage(enemy, player, damage):
	"""
	#1 En caso de que el personaje le tenga miedo al enemigo tiene un 50% de probabilidad de fallar,
	   y no tiene posibilidad de acertar golpes criticos
	#2 En caso de que el personaje le tenga furia al enemigo, SIEMPRE va a acertar golpes extraordinarios
	#3 En el caso de que no le tenga ni furia ni miedo al enemigo, hay un 12% de probabilidad de criticos
	"""
	if(player.fear != null and player.fear in enemy.race):
		var damageWithChangeToMiss = self._calculateDamageWithMiss(damage)
		enemy.takeDamage(damageWithChangeToMiss)
		self.attackText(player.position, damageWithChangeToMiss, "")
	elif(player.fury != null and player.fury in enemy.race):
		self.attackText(player.position, damage*3, "¡FURY ATTACK!")
		enemy.takeDamage(damage*3)
	else:
		_makeDamageWithChanceOfCritical(player, damage, enemy)
		
func _makeDamageWithChanceOfCritical(player, damage, enemy):
	randomize()
	var changeOfHit = randf()
	if(changeOfHit <= 0.12):
		EventBus.emit_signal("shake_camera",0.1,60,20,0)
		self.attackText(player.position, damage*2, "CRITICAL ")
		enemy.takeDamage(damage*2)
	else:
		EventBus.emit_signal("shake_camera",0.3,15,5,0)
		self.attackText(player.position, damage, "")
		enemy.takeDamage(damage)
	
func _calculateDamageWithMiss(damage):
	""" Se cambia la implementacion para tener una chanse del 35% de miss """
	randomize()
	var changeOfHit = randf()
	if(changeOfHit <= 0.35):
		return 0
	else:
		return damage
#	"""Armo un 50% de chances de fallar el golpe (pegar cero)"""
#	randomize()
#	var index = randi() % 2
#	var possibleValues = [0,damage]
#	var damageWithChangeToMiss = possibleValues[index]
#	return damageWithChangeToMiss

func randomTrait():
	"""PRECONDICION: se supone que como maximo se llama unas 2 veces esta función(para setear miedo/furia)"""
	randomize()
	var item = randi() % enemies.size()
	var result = enemies[item]
	enemies.erase(result)
	return result

func randomTraitWithOut(race):
	reset()
	enemies.erase(race)
	return randomTrait()

func attackText(playerPosition, damage, prefix):
	var floaty_text = floaty_text_scene.instance()
	var preValue
	floaty_text.position = playerPosition
	floaty_text.velocity = Vector2(rand_range(-50, 50), -100)
	floaty_text.modulate = Color(rand_range(0.7, 1), rand_range(0.7, 1), rand_range(0.7, 1), 1.0)

	preValue = damage
	if damage > 0:
		preValue = "+" + str(preValue)
		if prefix == "¡FURY ATTACK!":
			crit.play(0);
			EventBus.emit_signal("shake_camera",0.1,70,30,0)
		else:
			hit.play(0);
			EventBus.emit_signal("shake_camera",0.3,20,5,0)
	else:
		miss.play(0);
		EventBus.emit_signal("shake_camera",0.6,4,10,0)
		preValue = "¡fallaste!"
	floaty_text.text = prefix + str(preValue)
	add_child(floaty_text)
	
func reset():
	enemies = ["rat", "dog", "aberration" ]
